var bodyParser = require('body-parser');
var User = require('../models/user');
var CONSTANTS = require('../controllers/Constants');
var levelanswers = [];


//DASH BOARD
module.exports.gameboard=function (req,res)
{
    try{
        if(req.user.profile.currentlvl==1)
        {
                res.render('gamedboard', {
                    status_1: "You are in level1"
                });
        }
        else if(req.user.profile.currentlvl==2)
        {
            res.render('gamedboard', {
                status_2: "You completed in level1",
                });
        }
        else if(req.user.profile.currentlvl==3) {
            if (req.user.profile.qncompleted == 7) {
                res.render('gamedboard', {
                    status_4: "You comp in level3",
                });
            }
            else {
                res.render('gamedboard', {
                    status_3: "You are in level3",
                });
            }
        }
        else
        {
            req.flash("error_msg",'ERROR IN YOUR LEVEL CONTACT ADMIN');
            res.render('gamedboard');
        }
    }
    catch(err)
    {
        console.log(err);
    }
};

module.exports.levelone=function(req,res) {
    try {
        var input1 = req.body.lvl1_ans1 || null;
        var input2 = req.body.lvl1_ans2 || null;
        var input3 = req.body.lvl1_ans3 || null;
        var input4 = req.body.lvl1_ans4 || null;
        var input5 = req.body.lvl1_ans5 || null;
        if (input1) {
            if (input1.toLowerCase() == CONSTANTS.l1001) {
                levelanswers.push({"answer1": input1});
                User.getUserByUsername(req.user.username, function (err, user) {
                    if (err)
                        throw err;
                    if (req.user.profile.qncompleted == 0) {
                        var query = {username: user.username};
                        User.findOneAndUpdate(query, {
                            $set: {
                                profile: {
                                    qncompleted: 1, currentlvl:1
                                }
                            }
                        }, function (err, success) {
                            if (err)
                                throw err;
                            else {
                                console.log("updated the level 1");
                                console.log(req.user);
                            }
                        });
                        res.redirect('/games/q2');
                    }
                    else {
                        console.log('Query not completed');
                        req.flash('error_msg', 'You have completed this question... redirecting you to Game Dashboard');
                        res.redirect('/games/gamedboard');
                    }
                });
            }
            else {
                req.flash('error_msg', 'Wrong Answer!!!! Try Again ');
                res.redirect('q1');
            }
        }
        if (input2) {
            if (input2.toLowerCase() == CONSTANTS.l1002) {
                levelanswers.push({"answer2": input2});
                var temp = req.user.profile.qncompleted;
                if (temp == 1) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted: 2,
                                currentlvl: 1
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 2");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/q3');
                }
                else
                {
                    req.flash('error_msg', 'You have completed this question... redirecting you to Game Dashboard');
                    res.redirect('/games/gamedboard');
                }
            }
            else
            {
                req.flash('error_msg', 'Wrong Answer!!!! Try Again ');
                res.redirect('/games/q2');
            }
        }
        if (input3) {
            if (input3.toLowerCase() == CONSTANTS.l1003) {
                levelanswers.push({"answer3": input3});
                if (req.user.profile.qncompleted == 2) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted: 3,
                                currentlvl:1
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 3");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/q4');
                }
                else
                {
                    req.flash('error_msg', 'You have completed this question... redirecting you to Game Dashboard');
                    res.redirect('/games/gamedboard');
                }
            }
            else {
                req.flash('error_msg', 'Wrong Answer!!!! Try Again ');
                res.redirect('/games/q3');
            }
        }
        if (input4) {
            if (input4.toLowerCase() == CONSTANTS.l1004) {
                levelanswers.push({"answer4": input4});
                if (req.user.profile.qncompleted == 3) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted:4,
                                currentlvl:1
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 4");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/q5');
                }
                else {
                    req.flash('error_msg', 'You have completed this question... redirecting you to Game Dashboard');
                    res.redirect('/games/gamedboard');
                }

            }
            else
            {
                req.flash('error_msg', 'Wrong Answer!!!! Try Again ');
                res.redirect('/games/q4');
            }
        }
        if (input5) {
            if (input5.toLowerCase() == CONSTANTS.l1005) {
                levelanswers.push({"answer5": input5});
                if (req.user.profile.qncompleted == 4) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted: 5, currentlvl: 2
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 2");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/level1comp');
                }
                else {
                    req.flash('error_msg', 'You have completed this question... redirecting you to Game Dashboard');
                    res.redirect('/games/gamedboard');
                }
            }
            else {
                req.flash('error_msg', 'Wrong Answer!!!! Try Again ');
                res.redirect('/games/q5');
            }
        }
    }
    catch (err)
    {
        console.log(err);
    }};

module.exports.q1check=function(req,res){
    if(req.user.profile.qncompleted!=0)
    {
        res.render('game',{data: "Click below to your skip to your current question"
        });

    }
    else
    {
        res.render('game');
    }
};
module.exports.q1clickbutton=function (req,res) {
    try
    {
        var cur=req.user.profile.qncompleted;
        console.log(cur);
        if(cur==5)
        {
            res.redirect('/games/level1comp');
        }
        else if(cur==4)
        {
            res.redirect('/games/q5');
        }
        else if(cur==3)
        {
            res.redirect('/games/q4');
        }
        else if(cur==2)
        {
            res.redirect('/games/q3');
        }
        else if(cur==1)
        {
            res.redirect('/games/q2');
        }
        else if(cur==0)
        {
            res.redirect('/games/q1');
        }
        else
        {
            req.flash('error_msg','YOU HAVE COMPLETED THIS LEVEL');
            res.redirect('/games/game');
        }
    }
    catch(err)
    {
        console.log(err);
    }
};
module.exports.level2=function(req,res){
    try{
        if(req.user.profile.qncompleted!=5)
        {
            res.render('level2',{
                data: "You have completed this level"
            });

        }
        else
        {
            res.render('level2');
        }
    }
    catch(err)
    {
        console.log(err);
    }
};
module.exports.level2qn=function(req,res) {

    try{
        var input6= req.body.lvl2_ans;
        if(input6)
        {
            if(input6.toLowerCase()==CONSTANTS.l2001)
            {
                levelanswers.push({"answer6": input6});
                if (req.user.profile.qncompleted == 5) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted: 6, currentlvl: 3
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 2");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/level2comp');
                }
                else {
                    req.flash('error_msg', 'You could have finished this question!! Redirecting you to the DashBoard');
                    res.redirect('/games/gamedboard');
                }
            }
            else {
                req.flash('error_msg', 'Wrong answer!!! try again');
                res.redirect('/games/q6');
            }
        }
    }
    catch(err)
    {
        console.log(err);
    }
};
module.exports.level3=function (req,res) {
    try{
        if(req.user.profile.qncompleted!=6)
        {
            res.render('level3',{
                data: "You have completed this level"
            });

        }
        else
        {
            res.render('level3');
        }
    }
    catch(err)
    {
        console.log(err);
    }
};
module.exports.level3qn=function (req,res){
    try{
        var input7=req.body.lvl3_ans;
        if(input7){
            if(input7.toLowerCase()==CONSTANTS.l3001)
            {
                levelanswers.push({"answer7": input7});
                if (req.user.profile.qncompleted == 6) {
                    var user = req.user;
                    var query = {username: user.username};
                    User.findOneAndUpdate(query, {
                        $set: {
                            profile: {
                                qncompleted: 7, currentlvl: 3
                            }
                        }
                    }, function (err, success) {
                        if (err)
                            throw err;
                        else {
                            console.log("updated the level 3");
                            console.log(req.user);
                        }
                    });
                    res.redirect('/games/level3comp');
                }
                else {
                    req.flash('error_msg', 'You could have finished this question!! Redirecting you to the DashBoard');
                    res.redirect('/games/gamedboard');
                }
            }
            else {
                req.flash('error_msg', 'Wrong answer!!! try again');
                res.redirect('/games/q7');
            }
        }
    }
    catch(err)
    {
        console.log(err);
    }
};