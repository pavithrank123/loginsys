var express = require('express');
var router = express.Router();
var passport = require('passport');
var bodyParser = require('body-parser');
var LocalStrategy = require('passport-local').Strategy;
var usercont=require('../controllers/usercontroller');
var User = require('../models/user');
var authentic=require('./index.js');
var authentic1=require('./index.js');


// API GET CALLS 

router.get('/register',authentic1.aut,function(req, res){
	res.render('register');
});

router.get('/login',authentic1.aut,function(req, res){
	res.render('login');
});
router.get('/contact', function(req, res){
    res.render('contact');
});

router.get('/home', function(req, res){
    res.render('home');
});
router.get('/quiz',function (req,res) {
	res.render('quiz');
});

router.get('/logout', function(req, res){
	req.logout();

	req.flash('success_msg', 'You are logged out');

	res.redirect('/users/login');
});

router.get('/exit',function (req,res) {
	res.render('exit');
});

// post API calls

router.post('/register',function (req,res) {
    usercont.regusers(req,res);
});

router.post('/login', passport.authenticate('local', {successRedirect:'/', failureRedirect:'/users/login',failureFlash: true}),
  function(req, res) {
    res.redirect('/');
});

router.post('/quizsubmit',function (req,res) {
	usercont.quizcalc(req,res);
});

passport.use(new LocalStrategy(
  function(username, password, done) {
   User.getUserByUsername(username,function(err, user){
   	if(err) throw err;
   	if(!user){
   		return done(null, false, {message: 'Unknown User'});
   	}

   	User.comparePassword(password, user.password, function(err, isMatch){
   		if(err) throw err;
   		if(isMatch){
   			return done(null, user);
   		} else {
   			return done(null, false, {message: 'Invalid password'});
   		}
   	});
   });
  }));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

module.exports = router;