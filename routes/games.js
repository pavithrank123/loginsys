var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var usercont=require('../controllers/usercontroller');
var User = require('../models/user');
var authentic=require('./index.js');
var authentic1=require('./index.js');
var gamecont=require('../controllers/gamecontrollers');

// API GET CALLS
router.get('/game',authentic.auth,function(req,res){
    gamecont.q1check(req,res);
});

router.get('/q1',authentic.auth,function(req,res){
    res.render('q1');
});

router.get('/q2',authentic.auth,function(req,res){
    res.render('q2');
});

router.get('/q3',authentic.auth,function(req,res){
    res.render('q3');
});

router.get('/q4',authentic.auth,function(req,res){
    res.render('q4');
});

router.get('/q5',authentic.auth,function(req,res){
    res.render('q5');
});
router.get('/q6',authentic.auth,function(req,res){
    res.render('q6');
});
router.get('/q7',authentic.auth,function (req,res) {
    res.render('q7');
});
router.get('/level1comp',authentic.auth,function (req,res) {
    res.render('level1comp');
});
router.get('/level2',authentic.auth,function (req,res) {
    gamecont.level2(req,res);
});
router.get('/level3',authentic.auth,function (req,res) {
    gamecont.level3(req,res);
});
router.get('/gamedboard',authentic.auth,function (req,res) {
    gamecont.gameboard(req,res);
});
router.get('/level2comp',authentic.auth,function (req,res) {
    res.render('level2comp');
});
router.get('/level3comp',authentic.auth,function (req,res) {
    res.render('level3comp');
});



//API POST METHODS

router.post('/levelone',authentic.auth,function(req,res) {
    gamecont.levelone(req,res);
});
router.post('/game',authentic.auth,function(req,res){
    gamecont.q1clickbutton(req,res);
});
router.post('/level2',authentic.auth,function(req,res){
    gamecont.level2qn(req,res);
});
router.post('/level3',authentic.auth,function(req, res) {
   gamecont.level3qn(req,res);
});

module.exports =router;